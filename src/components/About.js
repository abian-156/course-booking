//[ACTIVITY]
import {Row, Col,} from 'react-bootstrap';


export default function About() {
	return(
		<Row className="p-5 aboutMe">
				<Col xs={12} md={4}>
					<h1 className="my-4"> About Me </h1>
					<h2>Grace M. Abian</h2>
					<h2>Full Stack Web Developer</h2>
					<p>I am a Full Stack Web Developer from Zuitt Coding Bootcamp</p>
					<h2>Contact Me</h2>
					<ul>
						<li>Email: graceabian10@gmail.com</li>
						<li>Mobile No. 0961-800-8556</li>
						<li>Address: Taguig City 4th District NCR, Philippines, 1630</li>
					</ul>
				</Col>
		</Row>
	
			
		
	);
};

