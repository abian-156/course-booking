//identify which component will be displayed.
import { useState, useEffect }from 'react';
import Hero  from './../components/Banner';
import CourseCard from './../components/CourseCard';
import  { Container } from 'react-bootstrap'


const bannerDetails = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of Courses'
}

//Catalog all Active courses
	//1. Container for each course taht we will retrieve fromthe database.
	//2.declare state for courses collectio: by default it is an empty array.
	//3. create a side effects taht will send a request to our backend api project.
	//4.pass down the retrieved resources indied the component.

export default function Courses() {

	//this array/storage will be used to save our course components for allactive courses.
	const [coursesCollection, setCourseCollection] =  useState([]);

	//create an effect using our effect hook, provide a list of dependecies
	//the effect taht we are going to create is fetching from the server.
	useEffect(() => {
		//fetch() => is a JS method which allow us to pass or create a request to an API.
		//syntax: fetch( <request URL>, {OPTIONS})
		//GET HTTP METHOD especially since we do not need to insert a req.body or pas token => NO NEED TO INSERT AN OPTIION PARAMETER

		//remember that once you sen a request to an endpoint a 'promise' is return as result.
		//A Promise has 3 states (fulfilled, reject, pending)
		//we need to able to handle the outcome of the promise.
		//we need to make the response usable on the frontend side. we need to convert it to a json format.
		//upon coverting fetched data to a json format a new promise will be executed.

		fetch('https://thawing-gorge-69376.herokuapp.com/courses')
		.then(res => res.json())
		.then(convertedData => {
			//console.log(convertedData);
			//we want the rsources to be displayed in tha page.
			//since the data is stored in an array storage, we will iterate the array explicitly get each document one by one.
			//there wil be multiple course casrd component that will render that corresponded to each document retrieved from the database. we need to itilize a storage to contain that cars taht will be rendered.
			//key attribute => as s reference of the same element/object.
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			})) 

		});
	},[]);

	return(
		<>
			<Hero bannerData={bannerDetails} />
			<Container>
				{coursesCollection}
			</Container>
		</>
	);
};
